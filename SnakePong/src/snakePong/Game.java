package snakePong;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

/**
 * The frame of the main game screen
 * @author Brent De Blaere
 */
public class Game extends JFrame{
	private static final long serialVersionUID = 7982197049720599150L; //generated
	private JPanel gamePanel;
	private int botDifficulty, fps;
	private Snake snake;

	/**
	 * Constructor
	 * @param botDifficulty difficulty of the paddle bot
	 * @param fps frames rendered per second
	 */
	public Game(int botDifficulty, int fps) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Snake Pong");
		setVisible(true);
		setSize(1080,710);
		this.botDifficulty = botDifficulty;
		this.fps = fps;
		this.gamePanel = new GamePanel();
		setContentPane(gamePanel);
		setResizable(false);
	}
	/**
	 * Close the Game frame
	 */
	private void close() {
		dispose();
	}
	/**
	 * Restart the game
	 */
	private void restart() {
		new Game(botDifficulty, fps);
		dispose();
	}
/*****************************************************************************************/
	/**
	 * Game panel
	 * @author Brent De Blaere
	 */
	private class GamePanel extends JPanel{
		private static final long serialVersionUID = -6426968938934719939L; //generated
		private Maze maze;
		private Paddle paddle;
		private Ball ball;
		private Graphics g;
		private Timer t;
		private final LinkedList<Pos> staticWalls;
		private LinkedList<Integer> pressed;
		private int countScored;
		private boolean lost;
		
		/**
		 * Constructor
		 */
		private GamePanel () {
			this.maze = new Maze(53,33);
			snake = new Snake(new Pos(41,14),5);
			this.paddle = new Paddle(14,18);
			this.ball = new Ball(new Pos(3,16));
			this.t = new Timer(1000/fps,new Loop());
			t.setInitialDelay(1000);
			this.staticWalls = new LinkedList<Pos>();
			this.countScored = 20;
			for (Pos p : maze.getWalls()) {
				staticWalls.prepend(p);
			}
			this.pressed = new LinkedList<Integer>();
			addKeyListener(new ReadInput());
			setFocusable(true);
			t.start();
			this.lost = false;
		}
		/**
		 * Draw the elements on the screen
		 */
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			this.g= g;
			g.setColor(Color.WHITE);
			setBackground(Color.BLACK);
			if (lost) g.setColor(Color.RED);
			draw(maze.getWalls());
			draw(paddle.getPos());
			if (countScored<10 && !lost)
				g.setColor(Color.GREEN);
			draw(snake.getPos());
			g.setColor(Color.BLUE);
			if (countScored++<10)
				g.setColor(Color.RED);
			draw(ball.getPos());
		}
	/**====================================================================================**/
		/**
		 * ActionListener for the Timer object
		 * @author Brent De Blaere
		 */
		private class Loop implements ActionListener {
			/**=
			 * Main game loop
			 */
			public void actionPerformed(ActionEvent e) {
				//WALLS
				LinkedList<Pos> walls = new LinkedList<Pos>(staticWalls);
				LinkedList<Pos> snakePos = snake.getPos();
				for (int i=0 ; i<snakePos.getSize(); i++)
					walls.prepend(snakePos.get(i));
				LinkedList<Pos> paddlePos = paddle.getPos();
				for (int i=0 ; i<paddlePos.getSize(); i++)
					walls.prepend(paddlePos.get(i));
				
				//GAME OVER
				if (snake.checkLost(walls)) {
					t.stop();
					lost=true;
					new GameOverFrame();
				}
				
				//GROW
				if (ball.checkLost()) {
					ball.reset();
					snake.grow();
					countScored = 0;
				}
				
				if (!lost) {
					//USER INPUT SNAKE
					if (pressed.contains(KeyEvent.VK_LEFT)) {
						snake.left();
						pressed.remove(KeyEvent.VK_LEFT);
					} else if (pressed.contains(KeyEvent.VK_RIGHT)) {
						snake.right();
						pressed.remove(KeyEvent.VK_RIGHT);
					} else {
						snake.forward();
					}
					
					paddle.robot(ball.getPos(), botDifficulty, walls);
					ball.move(walls);
				}
				repaint();
			}
		}
	/**====================================================================================**/
		/**
		 * Draw the all the positions of an array
		 * @param pos array of positions
		 */
		private void draw(Pos[] pos) {
			for(Pos p : pos) {
				int xPos = p.getX()*20;
				int yPos = p.getY()*20;
				g.fillRect(xPos, yPos, 20, 20);
			}
		}
		/**
		 * Draws all the positions of a LinkedList
		 * @param pos LinkedList of positions
		 */
		private void draw(LinkedList<Pos> pos) {
			for(int i=0 ; i<pos.getSize() ; i++) {
				int xPos = pos.get(i).getX()*20;
				int yPos = pos.get(i).getY()*20;
				g.fillRect(xPos, yPos, 20, 20);
			}
		}
		/**
		 * Draws a single position
		 * @param pos a position
		 */
		private void draw(Pos pos) {
			g.fillRect(pos.getX()*20, pos.getY()*20, 20, 20);
		}
	/**====================================================================================**/
		/**
		 * KeyListener for the user input
		 * @author Brent De Blaere
		 */
		public class ReadInput implements KeyListener {
			/**
			 * Log the key if a key is pressed
			 */
			public void keyPressed(KeyEvent e) {
				if (!pressed.contains(e.getKeyCode()))
				pressed.prepend(e.getKeyCode());
			}
			public void keyReleased(KeyEvent e) {
				return;
			}
			public void keyTyped(KeyEvent e) {
				return;
			}
		}
	/**====================================================================================**/
	}
/*****************************************************************************************/
/*****************************************************************************************/
	/**
	 * JFrame shown when the game ends
	 * @author Brent De Blaere
	 */
	private class GameOverFrame extends JFrame {
		private static final long serialVersionUID = -3221769610034666864L; //generated
		/**
		 * Constructor
		 */
		private GameOverFrame() {
			setContentPane(new GameOverPanel());
			setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
			setSize(250,250);
			setTitle("Game over");
			setVisible(true);
		}
		/**
		 * Game over panel
		 * @author Brent De Blaere
		 */
		private class GameOverPanel extends JPanel {
			private static final long serialVersionUID = 3759660598452981389L; //generated
				private JButton mainMenu, tryAgain;
				/**
				 * Constructor
				 */
				public GameOverPanel() {
					setBackground(Color.BLACK);
					
					setLayout(null);
					JLabel gameOverLabel = new JLabel("<html><h1>Game over</h1></html>");
					gameOverLabel.setBounds(30, 10, 500, 30);
					gameOverLabel.setForeground(Color.WHITE);
					add(gameOverLabel);
					
					JLabel score = new JLabel("<html><h2>Score: "+snake.getPos().getSize()+"</h2></html>");
					score.setBounds(30,40,500,30);
					score.setForeground(Color.WHITE);
					add(score);
					
					this.mainMenu = new JButton("Main Menu");
					mainMenu.addActionListener(new MainMenu());
					mainMenu.setBounds(30,100,120,25);
					add(mainMenu);
					
					this.tryAgain = new JButton("Try again");
					tryAgain.addActionListener(new TryAgainButtonHandler());
					tryAgain.setBounds(30,130,120,25);
					add(tryAgain);
				}
				/**
				 * ActionListener for the try again button
				 * @author Brent De Blaere
				 */
				private class TryAgainButtonHandler implements ActionListener {
					public void actionPerformed(ActionEvent e) {
						restart();
						dispose();
					}
				}
				/**
				 * ActionListener for the try again button
				 * @author Brent De Blaere
				 */
				private class MainMenu implements ActionListener {
					public void actionPerformed(ActionEvent e) {
						new SnakePong(botDifficulty, fps);
						close();
						dispose();
					}
				}
			}
	}
/*****************************************************************************************/
}
