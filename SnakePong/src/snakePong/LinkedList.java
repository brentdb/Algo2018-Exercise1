package snakePong;
/**
 * Class that defines a single LinkedList
 * @author Brent De Blaere
 *
 * @param <E>Type of the element of witch a LinkedList is composed
 */
public class LinkedList<E>{
	private int size;
	private Node head;
	/**
	 * Constructor
	 * @param element The fist (head) element of the LinkedList
	 */
	public LinkedList(E element) {
		this.size=1;
		this.head = new Node(element);
	}
	/**
	 * Constructor
	 * Creates an empty LinkedList
	 */
	public LinkedList() {
		this.size=0;
		this.head = null;
	}
	/**
	 * Constructor
	 * Creates a LinkedList from an existing LinkedList
	 * @param ll the LinkedList of witch a new LinkedList should be created
	 */
	public LinkedList(LinkedList<E> ll) {
		this(ll.getHead());
		for (int i=1 ; i<ll.getSize() ; i++) {
			prepend(ll.get(i));
		}
	}
	/**
	 * Constructor
	 * @param node The first (head) Node of the LinkedList
	 */
	private LinkedList(Node node) {
		this.head = node;
		this.size = count(node);
	}
	/**
	 * Counts the number of Nodes in the LinkedList
	 * @param node cursor
	 * @param count
	 * @return the number of Nodes
	 */
	private int count(Node node, int count) {
		if (node==null) return count;
		return count(node.getNext(),++count);
	}
	/**
	 * Conts the number of Nodes in the LinkedList
	 * @param node cursor
	 * @return the number of Nodes
	 */
	private int count(Node node) {
		return count(node, 0);
	}
	/**
	 * @return the first (head) element of the LinkedList
	 */
	public E getHead() {
		return head.getElement();
	}
	/**
	 * @return a LinkedList of the elements after the head Node
	 */
	public LinkedList<E> getTail(){
		return new LinkedList<E>(head.getNext());
	}
	/**
	 * @param index
	 * @return the element at the index provided
	 */
	public E get(int index) {
		return get(index, this, 0);
	}
	/**
	 * @param index
	 * @param rest
	 * @param count
	 * @return the element at the index provided
	 */
	private E get(int index,LinkedList<E> rest, int count) {
		if (index==count) return rest.getHead();
		return get(index,rest.getTail(),++count);
	}
	/**
	 * @return the size of the LinkedList
	 */
	public int getSize() {
		return size;
	}
	/**
	 * Adds an element to the front of the LinkedList
	 * @param element
	 */
	public void prepend(E element) {
		this.head = new Node(element,head);
		size++;
	}
	/**
	 * @param toFind
	 * @return true if the LinkedList contains the toFind element
	 */
	public boolean contains (E toFind) {
		return contains(toFind, head);
	}
	/**
	 * @param toFind
	 * @param cursor
	 * @return true if the LinkedList contains the toFind element
	 */
	private boolean contains(E toFind, Node cursor) {
		if (toFind==null || cursor==null) return false;
		if (toFind==cursor.getElement()) return true;
		return contains(toFind,cursor.next);
	}
	/**
	 * Search a Node in the LinkedList
	 * @param toFind
	 * @param cursor
	 * @return the Node
	 */
	private Node search(E toFind, Node cursor) {
		if (toFind==null || cursor==null) return null;
		if (toFind==cursor.getElement()) return cursor;
		return search(toFind, cursor.next);
	}
	/**
	 * Search the Node before an element
	 * @param toFind
	 * @param cursor
	 * @return the Node
	 */
	private Node searchNodeBefore(E toFind, Node cursor) {
		if (toFind==null || cursor==null || cursor.getNext()==null) return null;
		if (cursor.getNext().getElement().equals(toFind)) return cursor;
		return searchNodeBefore(toFind, cursor.getNext());
	}
	/**
	 * Remove an element from the LinkedList
	 * @throws NullPointerException if the LinkedList doesn't contain the element.
	 * @param element to remove
	 */
	public void remove(E element) {
		Node toBeRemoved;
		if (head.getElement().equals(element)) {
			toBeRemoved = search(element,head);
			head = toBeRemoved.getNext();
		} else {
			Node before = searchNodeBefore(element, head);
			toBeRemoved = before.getNext();
			before.setNext(toBeRemoved.getNext());
		}
		toBeRemoved.setNext(null);
		size--;
	}
	/**
	 * @return true if the LinkedList is empty
	 */
	public boolean isEmpty() {
		return size==0;
	}
	/**
	 * Class that defines a Node in a LinkedList
	 * @author Brent De Blaere
	 */
	private class Node{
		private E element;
		private Node next;
		/**
		 * Constructor
		 * @param element
		 * @param next
		 */
		public Node(E element, Node next) {
			this.element = element;
			this.next = next;
		}
		/**
		 * Constructor of the last Node in a LinkedList
		 * @param element
		 */
		public Node(E element) {
			this(element,null);
		}
		/**
		 * @return the element in the Node
		 */
		public E getElement() {
			return element;
		}
		/**
		 * @return the next Node
		 */
		public Node getNext() {
			return next;
		}
		/**
		 * Set the next Node
		 * @param next
		 */
		public void setNext(Node next) {
			this.next = next;
		}
	}

}
