package snakePong;

/**
 * Class for the Snake object
 * 
 * @author Brent De Blaere
 */
public class Snake {

	private LinkedList<Pos> pos;
	private boolean willGrow;
	/**
	 * Constructor 
	 * @throws IndexOutOfBoundsException if length is smaller than 2
	 * @param startPos the start position of the Snake
	 * @param length the length of the Snake
	 */
	public Snake (Pos startPos, int length) {
		if (length<=1) throw new IndexOutOfBoundsException("Length must be at least 2");
		int x = startPos.getX();
		int y = startPos.getY()+length-1;
		this.pos = new LinkedList<Pos>();
		for (int i=0 ; i<length ; i++) {
			this.pos.prepend(new Pos(x,y--));
		}
		this.willGrow = false;
	}
	/**
	 * Move the snake forward 
	 */
	public void forward() {
		move(0);
	}
	/**
	 * Move the snake to the left (relative to it's current direction)
	 */
	public void left() {
		move(-1);
	}
	/**
	 * Move the snake to the right (relative to it's current direction)
	 */
	public void right() {
		move(1);
	}
	/**
	 * Move the snake in a certain direction (relative to it's current direction)
	 * @param dir the direction in witch the snake should move (-1 0 or 1)
	 */
	private void move(int dir){
		//Determine position of head
		Pos pos0 = pos.getHead();
		Pos pos1 = pos.get(1);
		Pos headPos;
		if (pos0.getY()<pos1.getY())						//The snake moves up
			headPos = new Pos(pos0.getX()+dir,pos0.getY()-1+Math.abs(dir));
		else if (pos0.getY()>pos1.getY())					//The snake moves down
			headPos = new Pos(pos0.getX()-dir,pos0.getY()+1-Math.abs(dir));
		else if (pos0.getX()>pos1.getX())					//The snake moves to the right
			headPos = new Pos(pos0.getX()+1-Math.abs(dir),pos0.getY()+dir);
		else												//The snake moves to the left
			headPos = new Pos(pos0.getX()-1+Math.abs(dir),pos0.getY()-dir);
		pos.prepend(headPos);
		
		if (!willGrow) {
			pos.remove(pos.get(pos.getSize()-1));
			
		} else {
			willGrow = false;
		}
	}
	/**
	 * Let the snake grow the next time it moves
	 */
	public void grow() {
		willGrow = true;
	}
	/**
	 * Check if the snake hits any walls
	 * @param walls all the walls it can hit
	 * @return true if the Snake hits a wall
	 */
	public boolean checkLost(LinkedList<Pos> walls) {
		boolean foundOnce = false; //De eerste gevonden waarde is de kop van de slang zelf
		for (int i=0 ; i<walls.getSize() ; i++) {
			if (pos.getHead().equals(walls.get(i))) {
				if (foundOnce)
					return true;
				foundOnce = true;
			}
		}
		return false;
	}
	/**
	 * @return a LinkedList of the positions of the Snake
	 */
	public LinkedList<Pos> getPos() {
		return pos;
	}
}
