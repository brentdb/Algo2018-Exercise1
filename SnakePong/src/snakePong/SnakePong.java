package snakePong;

import java.awt.Color;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;
/**
 * Frame to show when the game is run
 * Contains the main method
 * 
 * @author Brent De Blaere
 */
public class SnakePong extends JFrame{
	private static final long serialVersionUID = -1510128862458510278L; //generated
	private JPanel panel;
	/**
	 * Constructor
	 * @param botDifficulty difficulty of the paddle bot
	 * @param fps number of frames rendered per second
	 */
	public SnakePong(int botDifficulty, int fps) {
		this.panel = new Menu(botDifficulty, fps);
		setContentPane(panel);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(700,300);
		setTitle("Snake Pong");
		setVisible(true);
	}
	/**
	 * Constructor
	 */
	public SnakePong() {
		this(80,15);
	}
	/**
	 * Main method
	 * Runs when the program starts
	 * @param args
	 */
	public static void main(String[] args) {
		new SnakePong();
	}
	/**
	 * Panel witch shows the main Menu
	 * 
	 * @author Brent De Blaere
	 */
	private class Menu extends JPanel{
		private static final long serialVersionUID = -4383591314954125587L; //generated
		private JButton start;
		private JSlider botSlider, speedSlider;
		private int botDifficulty, fps;
		private JLabel botLabel, speedLabel;
		/**
		 * Constructor
		 * @param botDifficulty difficulty of the paddle bot
		 * @param fps number of frames rendered per second
		 */
		private Menu(int botDifficulty, int fps) {			
			setBackground(Color.BLACK);
			setLayout(null);
			this.botDifficulty = botDifficulty;
			this.fps = fps;
			
			JLabel welcome = new JLabel("<html><h1>Welcome to Sake Pong</h1></html>");
				welcome.setBounds(30,10,300,50);
				welcome.setForeground(Color.white);
				add(welcome);
				
			JLabel botDiff = new JLabel("Bot difficulty:");
				botDiff.setBounds(70,60,150,20);
				botDiff.setForeground(Color.white);
				add(botDiff);
				
			this.botSlider = new JSlider(JSlider.HORIZONTAL, 0, 100, botDifficulty);
				botSlider.addChangeListener(new BotDiff());
				botSlider.setMajorTickSpacing(10);
				botSlider.setPaintTicks(true);
				botSlider.setPaintLabels(true);
				botSlider.setBounds(20,80,240,50);
				botSlider.setForeground(Color.white);
				botSlider.setBackground(Color.BLACK);
				add(botSlider);
			
			this.botLabel = new JLabel(botDifficulty+"%");
				botLabel.setBounds(270,60,50,30);
				botLabel.setForeground(Color.white);
				add(botLabel);
				
			JLabel speed = new JLabel("Speed:");
				speed.setBounds(70,140,150,20);
				speed.setForeground(Color.white);
				add(speed);
			
			this.speedSlider = new JSlider(JSlider.HORIZONTAL, 1, 30, fps);
				speedSlider.addChangeListener(new SpeedDiff());
				speedSlider.setMajorTickSpacing(3);
				speedSlider.setPaintTicks(true);
				speedSlider.setPaintLabels(true);
				speedSlider.setBounds(20,160,240,50);
				speedSlider.setForeground(Color.white);
				speedSlider.setBackground(Color.BLACK);
				add(speedSlider);
			
			this.speedLabel = new JLabel(fps+"fps");
			speedLabel.setBounds(270,140,50,30);
			speedLabel.setForeground(Color.white);
			add(speedLabel);
				
				
			String instText = "<html><center>"
					+ "<h2>Instructions</h2>"
					+ "<p>Press LEFT ARROW to move left<br/>"
					+ "Press RIGHT ARROW to move right.</p>"
					+ "</center></html>";
			JLabel instructions = new JLabel(instText);
				instructions.setBounds(400,10,300,80);
				instructions.setForeground(Color.white);
				add(instructions);
				
			this.start = new JButton("Click to start");
				start.addActionListener(new StartButtonHandler());
				start.setBounds(380,200,240,20);
				add(start);
		}
		/**
		 * ChangeListener for the botDifficulty
		 * @author Brent De Blaere
		 */
		private class BotDiff implements ChangeListener{
			public void stateChanged(ChangeEvent e) {
				botDifficulty = (int)((JSlider)e.getSource()).getValue();
				botLabel.setText(botDifficulty+"%");
			}
		}
		/**
		 * ChangeListener for the fps
		 * @author Brent De Blaere
		 */
		private class SpeedDiff implements ChangeListener {
			public void stateChanged(ChangeEvent e) {
				fps = (int)((JSlider)e.getSource()).getValue();
				speedLabel.setText(fps+"fps");
			}
		}
		/**
		 * ActionListener for the Button
		 * 
		 * @author Brent De Blaere
		 */
		private class StartButtonHandler implements ActionListener{
			public void actionPerformed (ActionEvent e) {
				new Game(botDifficulty, fps);
				dispose();
			}
		}
	}

}
