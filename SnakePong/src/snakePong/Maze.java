package snakePong;

/**
 * Class for the Maze object
 * 
 * @author Brent De Blaere
 */
public class Maze {

	private final int width, height;
	private Pos[] walls;
	/**
	 * Constructor
	 * @param width of the Maze
	 * @param height of the Maze
	 */
	public Maze(int width, int height) {
		this.width = width;
		this.height = height;
		int size = 2*width + 2*(height-2);
		walls = new Pos[size];
		
		for (int i=0; i<width; i++) {
			walls[i]=new Pos(i,0);//Bovenste muur
			walls[i+width] = new Pos(i,height-1);//Onderste muur
		}
		for (int i=0 ; i<height-2 ; i++) {
			walls[2*width+i] = new Pos(width-1,i+1);//rechtse muur
			walls[2*width+height-2+i] = new Pos(-1,i+1);//Onzichtbare linkse muur
		}
	}
	/**
	 * @return width of the Maze
	 */
	public int getWidth() {
		return width;
	}
	/**
	 * @return height of the Maze
	 */
	public int getHeight() {
		return height;
	}
	/**
	 * @return an array of the positions of the walls of the Maze
	 */
	public Pos[] getWalls() {
		return walls;
	}
}
