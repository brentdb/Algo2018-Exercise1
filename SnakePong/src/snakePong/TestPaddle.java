package snakePong;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class TestPaddle {

	private Paddle p1;
	@BeforeEach
	void setUp() throws Exception {
		p1 = new Paddle(5,8);
	}
	
	@Test
	void testConstructor() {
		assertTrue(5 == p1.getPosY()[0]);
		assertTrue(6 == p1.getPosY()[1]);
		assertTrue(8 == p1.getPosY()[3]);
		assertTrue(4 == p1.getPosY().length);
	}

	@Test
	void testDown() {
		p1.down(new LinkedList<Pos>());
		assertTrue(6 == p1.getPosY()[0]);
		assertTrue(9 == p1.getPosY()[3]);
	}
	
	@Test
	void testUp() {
		p1.up(new LinkedList<Pos>());
		assertTrue(4 == p1.getPosY()[0]);
		assertTrue(7 == p1.getPosY()[3]);
	}
	
}
