# SnakePong

An implementation of the game SnakePong

## How to run the game

Open the Snake Pong Brent _ V1.0.jar runnable to start the game.

## Built With

* [Eclipse](https://www.eclipse.org/) - The programming environment

## Authors

* **Brent De Blaere** - [brentdb](https://gitlab.com/brentdb)

## License

Open source. Free to use, copy adapt and share.

