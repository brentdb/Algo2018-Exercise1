package snakePong;

/**
 * Class for the Paddle object
 * 
 * @author Brent De Blaere
 */
public class Paddle {
	
	private int[] posY;
	private final int posX = 2;

	/**
	 * Constructor
	 * @throws IndexOutOfBoundsException when Ymin is larger than Ymax
	 * @param yMin minimum y coordinate of the Paddle
	 * @param yMax maximum y coordinate of the Paddle
	 */
	public Paddle(int yMin, int yMax) {
		if (yMin>yMax) throw new IndexOutOfBoundsException("Ymin can't be larger than Ymax");
		int size = yMax-yMin+1;
		this.posY = new int[size];
		for (int i=0 ; i<size ; i++) {
			this.posY[i] = yMin++;
		}
	}
	/**
	 * Moves the Paddle up
	 */
	public void up(LinkedList<Pos> walls) {
		boolean stuck = false;
		for (int i=0 ; i<walls.getSize() ; i++)
			if (posY[0]-1==walls.get(i).getY() && posX==walls.get(i).getX()) stuck = true;
		if (!stuck)
			for (int i=0 ; i<posY.length ; i++)
				posY[i]--;
	}
	/**
	 * Moves the Paddle down
	 */
	public void down(LinkedList<Pos> walls) {
		boolean stuck = false;
		for (int i=0 ; i<walls.getSize(); i++)
			if (posY[posY.length-1]+1==walls.get(i).getY() && posX==walls.get(i).getX()) stuck = true;
		if (!stuck)
			for (int i=0 ; i<posY.length ; i++)
				posY[i]++;
	}
	/**
	 * @return an array of the y coordinates of the Paddle
	 */
	public int[] getPosY(){
		return posY;
	}
	/**
	 * @return the x coordinate of the Paddle
	 */
	public int getPosX() {
		return posX;
	}
	/**
	 * @return a LinkedList of the positions of the Paddle
	 */
	public LinkedList<Pos> getPos() {
		LinkedList<Pos> pos = new LinkedList<Pos>();
		for (int i=0 ; i<posY.length ; i++) {
			pos.prepend(new Pos(posX, posY[i]));
		}
		return pos;
	}
	/**
	 * Makes the paddle move on its own
	 * @param ballPos the position of the ball
	 * @param strenght the strenght of the ball (0-100)
	 * @param walls the positions of the walls
	 */
	public void robot(Pos ballPos, int strenght, LinkedList<Pos> walls) {
		int rand = (int) (Math.random() * 101);
		if (rand<=strenght) {
			if (ballPos.getY()<posY[2])
				up(walls);
			else if (ballPos.getY()>posY[2])
				down(walls);
		} else {
			double r = Math.random();
			if (r<=0.3) {
				down(walls);
			} else if (r>=0.7) {
				up(walls);
			}
		}
	}
}
