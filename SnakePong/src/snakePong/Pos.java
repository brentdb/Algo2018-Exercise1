package snakePong;
/**
 * Defines a position of an element
 * 
 * @author Brent De Blaere
 */
public class Pos {

	private int x;
	private int y;
	/**
	 * Constructor
	 * @param x coordinate
	 * @param y coordinate
	 */
	public Pos(int x, int y) {
		this.x = x;
		this.y = y;
	}
	/**
	 * @return the x coordinate
	 */
	public int getX() {
		return x;
	}
	/**
	 * @return the y coordinate
	 */
	public int getY() {
		return y;
	}
	/**
	 * Check if a Position is equal to this position
	 * @param ref the position to compare
	 * @return true if the ref Pos equals this Pos
	 */
	public boolean equals(Pos ref){
		if (x==ref.getX() && y==ref.getY()) return true;
		return false;
	}
}
