package snakePong;

import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestBall {

	private Ball blHor, blUp, blDn;
	private LinkedList<Pos> wallRight, wallCeil, wallFloor, wallDiag;
	@BeforeEach
	void setUp() throws Exception {
		blHor = new Ball(new Pos(5,5),1,0);
		blUp = new Ball(new Pos(5,5),1,-1);
		blDn = new Ball(new Pos(5,5),1,1);
		wallRight = new LinkedList<Pos>(new Pos(6,5));
		wallCeil = new LinkedList<Pos>(new Pos(5,4));
		wallFloor = new LinkedList<Pos>(new Pos(5,6));
		wallDiag = new LinkedList<Pos>(new Pos(6,4));
	}
//System.out.println(blUp.getPos().getX()+" "+blUp.getPos().getY());
	@Test
	void testMove() throws Exception{
		//Right wall
		for (int i=0; i<100 ; i++) {
			setUp();
			blHor.move(wallRight);
			assertTrue(new Pos(4,5).equals(blHor.getPos())||
					new Pos(4,4).equals(blHor.getPos())||
					new Pos(4,6).equals(blHor.getPos()));
			
			blUp.move(wallRight);
			assertTrue(new Pos(4,5).equals(blUp.getPos()) ||
				new Pos(4,4).equals(blUp.getPos()));
			
			blDn.move(wallRight);
			assertTrue(new Pos(4,5).equals(blDn.getPos()) ||
					new Pos(4,6).equals(blDn.getPos()));
		}
		//Ceiling
		setUp();
		blHor.move(wallCeil);
		assertTrue(new Pos(6,6).equals(blHor.getPos()));
		blUp.move(wallCeil);
		assertTrue(new Pos(6,6).equals(blUp.getPos()));
		//Floor
		setUp();
		blHor.move(wallFloor);
		assertTrue(new Pos(6,4).equals(blHor.getPos()));
		blDn.move(wallFloor);
		assertTrue(new Pos(6,4).equals(blDn.getPos()));
		//diagonal wall
		setUp();
		blUp.move(wallDiag);
		assertTrue(new Pos(4,6).equals(blUp.getPos()));
		assertTrue(-1==blUp.getDir()[0]);
		//Stuck between left and right wall
		setUp();
		wallRight.prepend(new Pos(4,5));
		blHor.move(wallRight);
		assertTrue(new Pos(5,5).equals(blHor.getPos()));
		blUp.move(wallRight);
		assertTrue(new Pos(5,5).equals(blUp.getPos()));
		blDn.move(wallRight);
		assertTrue(new Pos(5,5).equals(blDn.getPos()));
		//Stuck between ceiling and floor
		setUp();
		wallCeil.prepend(wallFloor.getHead());
		blHor.move(wallCeil);
		assertTrue(new Pos(6,5).equals(blHor.getPos()));
		blUp.move(wallCeil);
		assertTrue(new Pos(6,5).equals(blUp.getPos()));
		blDn.move(wallCeil);
		assertTrue(new Pos(6,5).equals(blDn.getPos()));
		//In corner
		setUp();
		wallRight.prepend(wallCeil.getHead());
		blHor.move(wallRight);
		assertTrue(new Pos(4,6).equals(blHor.getPos()));
		blUp.move(wallRight);
		assertTrue(new Pos(4,6).equals(blUp.getPos()));
		blDn.move(wallRight);
		assertTrue(new Pos(4,6).equals(blDn.getPos()));
	}
	

}
