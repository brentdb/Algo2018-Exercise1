package snakePong;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestSnake {

	private Snake s1, s2;
	@BeforeEach
	void setUp() throws Exception {
		s1 = new Snake(new Pos(10,8),4);
		s2 = new Snake(new Pos(5,4),5);
	}
	@Test
	void testConstructor() {
		assertTrue(new Pos(10,8).equals(s1.getPos().get(0)));
		assertTrue(new Pos(10,9).equals(s1.getPos().get(1)));
		assertTrue(new Pos(10,11).equals(s1.getPos().get(3)));
		assertTrue(4==s1.getPos().getSize());
	}
	void moveS2() {
		s2.forward();
		s2.forward();
		s2.right();
		s2.forward();
		s2.left();
	}
	@Test
	void testMove() {
		moveS2();
		assertTrue(new Pos(7,1).equals(s2.getPos().getHead()));
		assertTrue(new Pos(7,2).equals(s2.getPos().get(1)));
		assertTrue(new Pos(6,2).equals(s2.getPos().get(2)));
		assertTrue(new Pos(5,2).equals(s2.getPos().get(3)));
		assertTrue(new Pos(5,3).equals(s2.getPos().get(4)));
	}	
	@Test
	void testGrow() {
		s2.grow();
		moveS2();
		assertTrue(new Pos(5,4).equals(s2.getPos().get(5)));
		assertTrue(6==s2.getPos().getSize());
	}
	
//	@Test
//	void testCheckLost() {
//		assertFalse(s2.checkLost(s2.getPos()));
//		s2.left();
//		assertFalse(s2.checkLost(s2.getPos()));
//		s2.left();
//		assertFalse(s2.checkLost(s2.getPos()));
//		s2.left();
//		assertTrue(s2.checkLost(s2.getPos()));
//		
//	}

}
