package snakePong;
/**
 * Class for the Ball object
 * 
 * @author Brent De Blaere
 */
public class Ball {

	private Pos pos;
	private int[] dir;
	private final Pos startPos;
	
	/**
	 * Constructor
	 * @param startPos start position of the ball
	 * @param dirX  The x-direction in witch the ball is moving (-1, 0 or 1)
	 * @param dirY The y-direction in witch the ball is moving (-1, 0 or 1)
	 */
	public Ball (Pos startPos, int dirX, int dirY) {
		this.pos = startPos;
		this.startPos = startPos;
		this.dir = new int[] {dirX, dirY};
	}
	/**
	 * Constructor
	 * @param startPos start position of the ball
	 */
	public Ball(Pos startPos) {
		this(startPos, 1, (int)(Math.random()*3-1));
	}
	/**
	 * Checks if the ball exited the screen
	 * @return true if the ball exited the screen
	 */
	public boolean checkLost() {
		return pos.getX()<=1;
	}
	/**
	 * Makes the ball bounce of a wall
	 * @param walls list of all the walls
	 */
	private void bounce(LinkedList<Pos> walls) {
		boolean[] collisionArea = new boolean[8];
		for (int i=0 ; i<collisionArea.length ; i++)
			collisionArea[i] = false;
		/*
		 * 	[7]	[0]	[4]
		 * 	[3]	 O	[1]
		 * 	[6]	[2]	[5]
		 */
		for (int i=0 ; i<walls.getSize() ; i++) {
			Pos p = walls.get(i);
			int x = pos.getX();
			int y = pos.getY();
			if(p.equals(new Pos(x,y-1))) collisionArea[0] = true;
			if(p.equals(new Pos(x+1,y))) collisionArea[1] = true;
			if(p.equals(new Pos(x,y+1))) collisionArea[2] = true;
			if(p.equals(new Pos(x-1,y))) collisionArea[3] = true;
			if (p.equals(new Pos(x+1,y-1))) collisionArea[4] = true;
			if (p.equals(new Pos(x+1,y+1))) collisionArea[5] = true;
			if (p.equals(new Pos(x-1,y+1))) collisionArea[6] = true;
			if (p.equals(new Pos(x-1,y-1))) collisionArea[7] = true;
		}
		
		if (collisionArea[1] || collisionArea[3]) {
			if (collisionArea[1]) {
				dir[0] = -1;
				//System.out.println("<");
			}//<--
			if (collisionArea[3]) {
				dir[0] = 1;			
				//System.out.println(">");
			}//-->
			if (collisionArea[1] && collisionArea[3]) dir[0] = 0;	//---
			
			if (dir[1]==0) dir[1] = -1 + (int)(Math.random()*3);	//pure random up/dn/hor
			else dir[1] = dir[1]*(int)(Math.random()*2);			//dependent on current direction
		}	
		if (collisionArea[0]) dir[1] = 1;
		if (collisionArea[2]) dir[1] = -1;
		if (collisionArea[0] && collisionArea[2]) dir[1] = 0;
		

		if (!collisionArea[0] && !collisionArea[1] && !collisionArea[2] && !collisionArea[3]) {
			if (dir[0]==1) {
				if (collisionArea[4] && dir[1]==-1) {
					dir[0]=-1;
					dir[1]=1;
				} else if (collisionArea[5] && dir[1]==1) {
					dir[0]=-1;
					dir[1]=-1;
				}
			} else if (dir[0]==-1) {
				if (collisionArea[6] && dir[1]==1) {
					dir[0]=1;
					dir[1]=-1;
				} else if (collisionArea[7] && dir[1]==-1) {
					dir[0]=1;
					dir[1]=1;
				}
			}
		}
		
		
	}
	/**
	 * Moves the ball
	 * @param walls list of all the walls
	 */
	public void move(LinkedList<Pos> walls) {
		bounce(walls);
		if (dir[0]!=0)
			pos =new Pos(pos.getX()+dir[0],pos.getY()+dir[1]);
	}
	/**
	 * Resets the ball to it's start position an direction
	 */
	public void reset() {
		pos = startPos;
		dir[0]=1;
		dir[1]=(int)(Math.random()*3-1);
	}
	/**
	 * @return the position of the ball
	 */
	public Pos getPos() {
		return pos;
	}
	/**
	 * @return the direction of traveling of the ball
	 */
	public int[] getDir() {
		return dir;
	}	
}
